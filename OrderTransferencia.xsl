<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h1>Prueba:</h1>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="Transaction">
		<b>
			<br/>
			Fecha de transaccion:
			<xsl:value-of
				select="concat(substring(./Fecha, 9, 2), '-', substring(./Fecha, 6, 2), '-', substring(./Fecha, 1, 4))"></xsl:value-of>
			
			<br/>
			Nombre del titular de la cuenta de cuenta origen:
			<xsl:value-of select="NombreOrigen"></xsl:value-of>
			<br/>
			Numero de cuenta origen:
			<xsl:value-of select="CuentaOrigen"></xsl:value-of>
			<br/>
			Nombre del destinatario:
			<xsl:value-of select="NombreDestinatario"></xsl:value-of>
			<br/>
			Cuenta del destinatario:
			<xsl:value-of select="CuentaDestino"></xsl:value-of>
			<br/>
			Cbu del destinatario:
			<xsl:value-of
				select="concat('xxxxxxxxxxx', substring(./cbuDestinatario, 12, 11))"></xsl:value-of>
			<br/>
			Tipo de moneda:
			<xsl:if test="(Moneda) = 'Pesos Chilenos' ">
				1
			</xsl:if>
			<xsl:if test="(Moneda) = 'Pesos Argentinos'">
				2
			</xsl:if>
			<xsl:if test="(Moneda) = 'Dolar'">
				3
			</xsl:if>
			<br/>
			Importe a acreditarse:
			<xsl:value-of select="ceiling(Importe)"></xsl:value-of>
			<br/>
			Plazo de acreditacion de importe:
			<xsl:value-of select="PlazoAcreditacion"></xsl:value-of>
			<br/>
			Concepto de transferencia:
			<xsl:value-of select="Concepto"></xsl:value-of>
			<br/>
			Número de comprobante:
			<xsl:value-of select="NroComprobante"></xsl:value-of>
		</b>
		

		
	</xsl:template>
</xsl:stylesheet>